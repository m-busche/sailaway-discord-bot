import os
import errno
import requests
import time
import re
import json
from datetime import datetime, timedelta
# import graypy
import logging
import pandas as pd
from operator import itemgetter
import sailaway
from bs4 import BeautifulSoup
from csv2json import convert, load_csv
from dotenv import load_dotenv
load_dotenv(verbose=True)

# LOG_SERVER_ADDRESS = os.getenv('GRAYLOG_SERVER')
# LOG_SERVER_PORT = int(os.getenv('GRAYLOG_PORT'))
DATAPATH = os.getenv('DATAPATH')

# my_logger = logging.getLogger(__name__)
# my_logger.setLevel(logging.DEBUG)
# handler = graypy.GELFUDPHandler(LOG_SERVER_ADDRESS, LOG_SERVER_PORT)
# my_logger.addHandler(handler)

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def safe_open_w(path):
    ''' Open "path" for writing, creating any parent directories as needed.
    '''
    mkdir_p(os.path.dirname(path))
    return open(path, 'w')

def getRacesFromSarl():
    # my_logger.info('Requesting all races from SARL')
    url='https://sarl.ingenium.net.au/'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    divs = soup.find_all('div', class_='panel-body')
    missionsNo = []
    missionsName = []

    hrefs=divs[2].find_all('a')
    for h in hrefs:
        if 'racenr' in h.attrs['href']:
            if not 'JSON' in h.contents[0]:
                race = re.search(r'\d\d\d\d\d', h.attrs['href'])[0]        
                missionsNo.append(race)
                missionsName.append(h.contents[0])
    # my_logger.info(f'Received {len(missionsNo)} races')
    return(missionsNo, missionsName)


def races2Csv(missionsNo, missionsName):
    # my_logger.debug('Saving race list as CSV file')
    with safe_open_w(f'{DATAPATH}/races.csv') as outfile:
        outfile.write('"racenumber","racetitle"\n')
        for i in range(0, len(missionsNo) - 1):
            outfile.write(f'"{missionsNo[i]}","{missionsName[i]}"\n')

def csv2Json():
    # my_logger.debug('Converting CSV file to JSON file')
    with open(f'{DATAPATH}/races.csv') as r, safe_open_w(f'{DATAPATH}/races.json') as w:
        convert(r, w)

def saveAllLeaderboardsToJsonFiles():
    # my_logger.info('Requesting leader boards for all races from Sailaway API')
    with open(f'{DATAPATH}/races.json') as f:
        data = json.load(f)

    for race in data:
        # my_logger.info(f"Requesting data for race #{race['racenumber']}")
        lb = sailaway.getAPIData(race['racenumber'], 'lb')
        filename = f"lb_{race['racenumber']}.json"
        # my_logger.info(f'Writing leader board results to {filename}')
        with safe_open_w(f'{DATAPATH}/{filename}') as outfile:
            json.dump(lb, outfile)


def findRaces(playername: str):
    # my_logger.info(f'Searching leader boards for player {playername}')
    playerraces = []
    racefiles = os.listdir(DATAPATH)
    for f in racefiles:
        if f.endswith('.json') and f.startswith('lb_'):
            with open(f'{DATAPATH}/{f}') as lb_file:
                data = json.load(lb_file)
            for entity in data['leaderboard'][0]['results']:
                player = entity['usrname']
                if playername:
                    if player.lower() == playername.lower():
                        racename = data['leaderboard'][0]['title']
                        racenumber = data['leaderboard'][0]['nr']
                        rank = entity['rank']
                        try:
                            status = entity['status']
                        except:
                            status = 'racing'
                        # my_logger.debug(f'Found player in race {racenumber}')
                        playerraces.append(f'Race: {racenumber} - Title: {racename} - Rank: {rank} - Status: {status}')
                else:
                    playerraces.append(f'Race: {racenumber} - Title: {racename} - Rank: {rank} - Status: {status}')
                # pass
    if len(playerraces) == 0:
        # my_logger.debug(f'No race found for player {playername}')
        playerraces.append(f'No races found for player *{playername}*.')
    
    text = f'🎟️ Result of race-search for player *{playername}*:\n'
    for r in playerraces:
        text += f'{r}\n'

    return(text)


def updateRaces():
    deleteOldData()
    print('Starting scheduled race data update')
    missionsNo, missionsName = getRacesFromSarl()
    # missionsNo, missionsName = sailaway.getActiveRaces()
    races2Csv(missionsNo, missionsName)
    csv2Json()
    saveAllLeaderboardsToJsonFiles()


def deleteOldData():
    print('Deleting old data files')
    orpahnedFiles = os.listdir(DATAPATH)
    for item in orpahnedFiles:
        if item.endswith(".json"):
            os.remove(os.path.join(DATAPATH, item))
    pass

def raceNumberToTitle(racenr):
    filename = f'{DATAPATH}/races.json'
    with open(filename) as racelist:
        data = json.load(racelist)
    for race in data:
        r = str(race['racenumber'])
        if r == racenr:
            result = race['racetitle']
            pass
    return(result)


def raceTitleToNumber(raceTitleSeachtext):
    found = False
    result = ''
    seachtext = raceTitleSeachtext.lower()
    filename = f'{DATAPATH}/races.json'
    with open(filename) as racelist:
        data = json.load(racelist)
    for race in data:
        r = race['racetitle'].lower()
        if r.find(seachtext) != -1:
            found = True
            result += f"💡 Race {raceTitleSeachtext} matched for race #{race['racenumber']}.\n" \
                    f"SARL: https://sarl.ingenium.net.au/sarl?racenr={race['racenumber']}\n\n"
    if found == False:
        result = f'🤯 I have not found a race containing {raceTitleSeachtext}.'
    return(result)

def sendAllRaces():
    text = ''
    filename = f'{DATAPATH}/races.json'
    lastRequest = datetime.fromtimestamp(sailaway.creation_date(f'{filename}')).strftime("%m/%d/%Y, %H:%M:%S")
    
    with open(filename) as infile:
        data = json.load(infile)

    df = pd.DataFrame(data)
    df.columns = ['Race number', 'Race title']
    text = df.to_string(index=False)
    return(lastRequest, text)


def raceExist(racenr):
    result = False
    filename = f'{DATAPATH}/races.json'
    with open(filename) as f:
        data = json.load(f)
    
    for r in data:
        if r['racenumber'] == racenr:
            result = True
            pass
    return(result)
    

if __name__ == "__main__":
    # raceExist('34709')
    # saveAllLeaderboardsToJsonFiles()
    getRacesFromSarl()
    pass
